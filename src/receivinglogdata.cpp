#include "receivinglogdata.h"

ReceivingLogData::ReceivingLogData(const QList<QString> &headers, const QList<ReceivedItem> &data, QObject *parent)
    : QAbstractTableModel(parent)
{
    data_list = data;
    header_list = headers;
}

int ReceivingLogData::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return data_list.count();
}

int ReceivingLogData::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return header_list.count();
}

QVariant ReceivingLogData::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        return header_list.at(section);
    }

    return QVariant();
}

QVariant ReceivingLogData::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= data_list.size() || index.row() < 0) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        ReceivedItem item = data_list.at(index.row());

        return item.data(index.column());
    }
}

ReceivingLogData::~ReceivingLogData()
{

}
