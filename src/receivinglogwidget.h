#ifndef RECEIVINGLOGWIDGET_H
#define RECEIVINGLOGWIDGET_H

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QUrl>
#include <QDesktopServices>
#include <QSettings>
#include <QMessageBox>
#include "receivinglogdata.h"
#include "receiveditem.h"
#include "printableworksheet.h"

class ReceivingLogWidget : public QWidget
{
    Q_OBJECT
    Q_ENUMS(Column)
public:
    explicit ReceivingLogWidget(QWidget *parent = 0);

    enum Column { Department, Misc1, Location, Sku, Style, Size, Color, UPC, QtyReceived, Available, Brand, Description, NameDate };

    QList<QString> headers;
    QString rl_number;
    QSqlDatabase posim_db;
    ReceivingLogData *rl_data;
    QList<ReceivedItem> rl_items_list;

    QLabel* rl_label;
    QLabel* receiver_label;
    QLabel* vendor_label;
    QLabel* date_label;

    QLineEdit* rl_edit;
    QLineEdit* receiver_edit;
    QLineEdit* vendor_edit;
    QLineEdit* date_edit;

    QPushButton* print_button;

    QGridLayout* details_layout;
    QGroupBox* details_groupbox;

    QGridLayout* main_layout;

    PrintableWorksheet *worksheet;

signals:
    void rlDataReady();
    void dataReadyForPrint();

public slots:
    void getRLDetails();
    void getRLItems();
    void createDataModel();
    void printBackstockWorksheet();
    void openHtmlFile(QString path);

protected:
    void initDB();
    void initMembers();
    void initUI();
    void addItemData(QSqlRecord record);
};

#endif // RECEIVINGLOGWIDGET_H
