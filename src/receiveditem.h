#ifndef RECEIVEDITEM_H
#define RECEIVEDITEM_H

#include <QString>
#include <QVector>
#include <QVariant>

class ReceivedItem
{
public:
    explicit ReceivedItem(const QList<QString> &data, ReceivedItem *parent = 0);
    ~ReceivedItem();

    int columnCount() const;
    QVariant data(int column) const;

private:
    QList<QString> item_data;
};

#endif // RECEIVEDITEM_H
