#ifndef BARCODE_H
#define BARCODE_H

/*
 * Based off of Qt Barcode by adamgiacomelli
 * https://github.com/adamgiacomelli/Qt-barcode
 */

#include <QObject>
#include <QString>
#include <QPainter>
#include <QPixmap>
#include <QFont>
#include <QFontDatabase>
#include <QRect>
#include <QByteArray>
#include <QBuffer>
#include <QDebug>

#define CODE128_B_START 104
#define CODE128_STOP 106

class Barcode : public QObject
{
public:
    Barcode(QString sku);

    QString encodeBarcode(QString code);
    int calculateCheckCharacter(QString code);
    QPixmap createBarcodeImage(QString b_string);
    QString createBarcodeHtml(QPixmap pix);
    QString getBarcodeHtml();
    void setBarcodeFont();

    QFont barcode_font;
    QString barcode_string;
    QString barcode_html;

    int codeToChar(int code);
    int charToCode(int ch);
};

#endif // BARCODE_H
