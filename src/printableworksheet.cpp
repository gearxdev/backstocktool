#include "printableworksheet.h"

PrintableWorksheet::PrintableWorksheet(QWidget *parent) :
    QObject(parent)
{
    initMembers();
}

void PrintableWorksheet::initMembers()
{
    page_count = 1;
    initTemplates();
}

void PrintableWorksheet::initTemplates()
{
    html_template = "<!DOCTYPE html>"
                   "<html>"
                   "<head>"
                   "</head>"
                   "<body>"
                   "<h1 style='text-align:center; margin-top:0;'>BACKSTOCK WORKSHEET</h1>"
                   "<h3 style='text-align:center; margin-top:1;'>$details</h3>" // Put RL number here as well? Someplace prominent
                   "$pages"
                   "</body>"
                   "</html>";

    page_template =  "<div style='padding-left:20px;padding-top:$padding;'>"
                    "<table border style='border-width:2px;"
                    "border-spacing:2px;"
                    "border-style:outset;"
                    "border-collapse:collapse;"
                    "width:100%;'>"
                    "<tbody>"
                    "<tr>"
                    "<!--This is the header-->"
                    "$headers"
                    "</tr>"
                    "$lines"
                    "</tbody>"
                    "</table>"
                    "<h4 style='text-align:center;'>Page $pageNum</h4>"
                    "</div>";

    line_template = "<tr>"
                   "$receivedItem"
                   "</tr>";
}

void PrintableWorksheet::printFile()
{
    if (lines.count() != 0) addPage();

    QString html = html_template.replace("$pages", pages.join("\n"));

    bool success = writeHtmlToFile(html);

    if (success) {
        emit htmlSaved(file_path);
    } else {
        QMessageBox error_box(QMessageBox::Critical,
                           "ERROR",
                           "Error: failed to save file.");
        error_box.exec();
    }
}

void PrintableWorksheet::setFilePath(QString path, QString name)
{
    file_path = path + "/" + name;
}

QString PrintableWorksheet::getFilePath()
{
    return file_path;
}

void PrintableWorksheet::setHeaders(QList<QString> header_list)
{
    headers.clear();
    QString header_html;

    foreach (QString header, header_list) {
        headers.append(header);
        header_html.append("<td style='font-size:x-small;'>" + header + "</td>");
    }

    header_html.append("<td>Sku Barcode</td>");

    page_template = page_template.replace("$headers",header_html);
}

void PrintableWorksheet::setDetails(QStringList details)
{
    html_template = html_template.replace("$details", details.join(" - "));
}

void PrintableWorksheet::addPage()
{
    QString padding;
    QString current_page = page_template;

    if (page_count > 1) {
        current_page.prepend("<p style='page-break-after:always;'></p>");
        padding = "60px";
    } else {
        padding = "none";
    }

    current_page.replace("$lines", lines.join("\n"));
    current_page.replace("$pageNum", QString::number(page_count));
    current_page.replace("$padding", padding);

    pages.append(current_page);

    page_count++;
    lines.clear();
}

void PrintableWorksheet::addLine(QList<QString> item_data)
{
    QString item_html;
    QString line = line_template;
    QString sku = item_data.at(headers.indexOf("Sku"));

    foreach (QString data, item_data) {
        item_html.append("<td style='font-size:x-small;white-space:nowrap;padding:5px;'>"+data+"</td>");
    }

    Barcode barcode(sku);
    QString barcodeHtml = barcode.getBarcodeHtml();
    item_html.append("<td style='font-size:smaller; padding:5px;'>"+barcodeHtml+"</td>");

    line.replace("$receivedItem", item_html);

    lines.append(line);
}

bool PrintableWorksheet::writeHtmlToFile(QString html)
{
    QFile html_file(file_path);

    if (!html_file.open(QIODevice::WriteOnly)) {
        return false;
    }

    QTextStream stream(&html_file);
    stream << html;

    html_file.flush();
    html_file.close();

    return true;
}

PrintableWorksheet::~PrintableWorksheet()
{

}
