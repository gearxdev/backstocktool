#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QSettings>
#include <QStatusBar>
#include <QLabel>
#include "receivinglogwidget.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    ReceivingLogWidget* rl_widget;

private:
    void initMembers();
    void saveSettings();
    void loadSettings();
};

#endif // MAINWINDOW_H
