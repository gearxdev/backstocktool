#include "receivinglogwidget.h"

ReceivingLogWidget::ReceivingLogWidget(QWidget *parent) : QWidget(parent)
{
    initUI();
    initDB();
    initMembers();

    connect(rl_edit, SIGNAL(returnPressed()), this, SLOT(getRLDetails()));
    connect(print_button, SIGNAL(clicked(bool)), this, SLOT(getRLItems()));
    connect(this, SIGNAL(rlDataReady()), this, SLOT(createDataModel()));
    connect(this, SIGNAL(dataReadyForPrint()), this, SLOT(printBackstockWorksheet()));
}

void ReceivingLogWidget::initMembers()
{
    //Populate headers for data model
    headers.insert(Department,"Department");
    headers.insert(Misc1, "Misc1");
    headers.insert(Location,"Location");
    headers.insert(Sku,"Sku");
    headers.insert(Style, "Style");
    headers.insert(Size, "Size");
    headers.insert(Color, "Color");
    headers.insert(UPC, "UPC");
    headers.insert(QtyReceived, "Rcvd");
    headers.insert(Available,"Avail");
    headers.insert(Brand,"Brand");
    headers.insert(Description,"Description");
    headers.insert(NameDate, "LocationUpdate");
}

void ReceivingLogWidget::initUI()
{
    rl_label = new QLabel("Enter RL Number:", this);
    receiver_label = new QLabel("Receiver:", this);
    vendor_label = new QLabel("Vendor:", this);
    date_label = new QLabel("Date: ", this);

    rl_edit = new QLineEdit(this);
    receiver_edit = new QLineEdit(this);
    vendor_edit = new QLineEdit(this);
    date_edit = new QLineEdit(this);

    receiver_edit->setReadOnly(true);
    vendor_edit->setReadOnly(true);
    date_edit->setReadOnly(true);

    print_button = new QPushButton("Print Backstock Worksheet", this);

    details_layout = new QGridLayout();
    details_groupbox = new QGroupBox("ReceivingLogDetails:");

    details_layout->addWidget(receiver_label,0,0);
    details_layout->addWidget(receiver_edit,0,1);
    details_layout->addWidget(vendor_label,1,0);
    details_layout->addWidget(vendor_edit,1,1);
    details_layout->addWidget(date_label,2,0);
    details_layout->addWidget(date_edit,2,1);

    details_groupbox->setLayout(details_layout);

    main_layout = new QGridLayout(this);

    main_layout->addWidget(rl_label,0,0);
    main_layout->addWidget(rl_edit,0,1);
    main_layout->addWidget(details_groupbox,1,0,2,2);
    main_layout->addWidget(print_button,3,0,1,2);

    main_layout->setVerticalSpacing(30);
}

void ReceivingLogWidget::initDB()
{
    QString config_file_path = QDir::currentPath() + "/config/config.ini";

    QSettings db_settings(config_file_path, QSettings::IniFormat);

    db_settings.beginGroup("posim");
    posim_db = QSqlDatabase::addDatabase("QMYSQL");
    posim_db.setHostName(db_settings.value("host").toString());
    posim_db.setDatabaseName(db_settings.value("db_name").toString());
    posim_db.setUserName(db_settings.value("username").toString());
    posim_db.setPassword(db_settings.value("password").toString());
    posim_db.setPort(db_settings.value("port").toInt());
    posim_db.setConnectOptions("MYSQL_OPT_RECONNECT=1"); // Reconnect if mysql times out.
    db_settings.endGroup();

    if (!posim_db.open()) {
        QMessageBox err_box(QMessageBox::Critical,
                            "CONNECTION ERROR",
                            "Error: Failed to connect to database. Please make sure there is a config file at " + config_file_path);
        err_box.exec();
        qDebug() << "Error: Database not connected.";
    } else {
        qDebug() << "Database connected.";
    }
}

void ReceivingLogWidget::getRLDetails()
{
    rl_number = rl_edit->text();

    QString query_string = QString("SELECT receivinglogs.receivedDate, receivinglogs.receivedBy, purchaseorders.vendorName FROM receivinglogs JOIN purchaseorders ON receivinglogs.poId = purchaseorders.id WHERE receivinglogs.number = '%1';").arg(rl_number);

    QSqlQuery query = posim_db.exec(query_string);

    while (query.next()) {
        receiver_edit->setText(query.value("receivedBy").toString());
        vendor_edit->setText(query.value("vendorName").toString());
        date_edit->setText(query.value("receivedDate").toString());
    }
}

void ReceivingLogWidget::getRLItems()
{
    rl_items_list.clear();

    if (receiver_edit->text() != "") {
        QString query_string = QString("SELECT items.sku AS Sku, items.style AS Style, items.row AS Size, items.col AS Color, upcs.upc AS UPC, rllines.description AS Description, pdoclines.QtyIO AS Rcvd, itemqtys.qtyIO + itemqtys.qtyCommitted AS Avail, items.brand AS Brand, items.department AS Department, items.misc1 AS Misc1, SUBSTRING_INDEX(items.location, ' ', 1) AS Location, SUBSTRING_INDEX(items.location, ' ', -2) AS LocationUpdate FROM receivinglogs LEFT JOIN rllines ON receivinglogs.id = rllines.rlId LEFT JOIN items ON rllines.itemId = items.id LEFT JOIN upcs ON items.UpcId = upcs.id LEFT JOIN itemqtys ON items.id = itemqtys.itemId LEFT JOIN pdoclines ON rllines.id = pdoclines.parentLineId WHERE receivinglogs.number = '%1' AND pdoclines.QtyIO != 0 ORDER BY Department, Brand, Style;").arg(rl_number);

        QSqlQuery query = posim_db.exec(query_string);

        while (query.next()) {
            addItemData(query.record());
        }

        emit rlDataReady();
    } else {
        QMessageBox msg_box;
        msg_box.setText("There was an error.  Please make sure you have done the following:\n"
                       "1. Enter a valid processed RL number.\n"
                       "2. Press Enter key and confirm that it is the correct RL.");
        msg_box.exec();
    }
}

void ReceivingLogWidget::addItemData(QSqlRecord record)
{
    QList<QString> record_data;

    for (int i = 0; i < headers.count(); i++) {
        record_data.insert(i,record.value(headers.at(i)).toString());
    }

    ReceivedItem item(record_data);
    rl_items_list << item;
}

void ReceivingLogWidget::createDataModel()
{
    rl_data = new ReceivingLogData(headers, rl_items_list, this);

    emit dataReadyForPrint();
}


void ReceivingLogWidget::printBackstockWorksheet()
{
    worksheet = new PrintableWorksheet(this);

    connect(worksheet, SIGNAL(htmlSaved(QString)), this, SLOT(openHtmlFile(QString)));

    QString file_name = "Backstock_Worksheet_"+rl_number+"_"+QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".html";

    QDir dir;
    if (!QFile::exists(QDir::homePath()+"/Desktop/Backstock_Worksheets/")) {
        dir.mkdir(QDir::homePath()+"/Desktop/Backstock_Worksheets/");
    }

    QString date = QDate::currentDate().toString("MM-dd-yyyy");

    QStringList details;
    details << rl_number
            << vendor_edit->text()
            << receiver_edit->text()
            << date;

    QString file_path = QDir::homePath()+"/Desktop/Backstock_Worksheets";

    worksheet->setFilePath(file_path, file_name);
    worksheet->setDetails(details);
    worksheet->setHeaders(headers);

    for (int row = 0; row < rl_data->rowCount(); row++) {

        if (row != 0 && (row % worksheet->PAGE_BREAK_MARK) == 0) worksheet->addPage(); // add a page after certain number of rows

        QList<QString> row_data;
        for (int column = 0; column < rl_data->columnCount(); column++) {
            QModelIndex index = rl_data->index(row, column);
            QString data = rl_data->data(index).toString();
            row_data.append(data);
        }

        worksheet->addLine(row_data);

    }

    worksheet->printFile();

    QMessageBox save_msg;
    save_msg.setText("A copy of this backstock worksheet has been saved in " + file_path + ".");
    save_msg.exec();
}

void ReceivingLogWidget::openHtmlFile(QString path)
{
    QDesktopServices::openUrl(path);
}
