#ifndef UPDATECHECKER_H
#define UPDATECHECKER_H

#include <QSettings>
#include <QFile>
#include <QMessageBox>
#include <QDir>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSysInfo>
#include <QApplication>
#include <QDebug>
#include "qsimpleupdater.h"

class UpdateChecker
{
public:
    UpdateChecker();
    ~UpdateChecker();

    void checkForUpdates();

private:
    const QString CONFIG_FILE_PATH = QDir::currentPath() + "/config/config.ini";
    const QString UPDATES_FILE_PATH = QDir::currentPath() + "/definitions/BackstockTool_updates.json";
    QString updates_url;
    QSimpleUpdater *s_updater;

    void setApplicationVersion();
    void getUpdatesURL();
};

#endif // UPDATECHECKER_H
