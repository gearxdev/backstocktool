#ifndef RECEIVINGLOGDATA_H
#define RECEIVINGLOGDATA_H

#include <QAbstractTableModel>
#include <QVariant>
#include <QModelIndex>
#include <QStringList>
#include <QDebug>
#include "receiveditem.h"

class ReceivingLogData : public QAbstractTableModel
{
    Q_OBJECT
public:
    ReceivingLogData(const QList<QString> &headers, const QList<ReceivedItem> &data, QObject *parent = 0);
    ~ReceivingLogData();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    QList<ReceivedItem> data_list;
    QList<QString> header_list;
};

#endif // RECEIVINGLOGDATA_H
