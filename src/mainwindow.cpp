#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    loadSettings();

    setWindowTitle("Das Backstock Maschine");

    initMembers();
}

void MainWindow::initMembers()
{
    rl_widget = new ReceivingLogWidget(this);
    setCentralWidget(rl_widget);
}

void MainWindow::loadSettings()
{
    // Load window geometry from previous session (or default if no previous session)
    QSettings win_settings("Warehouse", "BackstockTool");
    win_settings.beginGroup("MainWindow");
    QRect default_rect(100,100,400,300);
    QRect win_rect = win_settings.value("position",default_rect).toRect();
    setGeometry(win_rect);
    win_settings.endGroup();
}

void MainWindow::saveSettings()
{
    // Save window geometry for next session
    QSettings win_settings("Warehouse", "BackstockTool");
    win_settings.beginGroup("MainWindow");
    win_settings.setValue("position", this->geometry());
    win_settings.endGroup();
}

MainWindow::~MainWindow()
{
    saveSettings();
}
