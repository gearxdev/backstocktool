#include "mainwindow.h"
#include "updatechecker.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("BackstockTool");

    MainWindow w;

    UpdateChecker *update_checker = new UpdateChecker();
    update_checker->checkForUpdates();

    w.show();

    return a.exec();
}
