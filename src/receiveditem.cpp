#include "receiveditem.h"

ReceivedItem::ReceivedItem(const QList<QString> &data, ReceivedItem *parent)
{
    item_data = data;
}

int ReceivedItem::columnCount() const
{
    return item_data.count();
}

QVariant ReceivedItem::data(int column) const
{
    return item_data.value(column);
}

ReceivedItem::~ReceivedItem()
{
}
