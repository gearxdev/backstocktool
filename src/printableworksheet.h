#ifndef PRINTABLEWORKSHEET_H
#define PRINTABLEWORKSHEET_H

#include <QDir>
#include <QDate>
#include <QPixmap>
#include <QByteArray>
#include <QObject>
#include <QMessageBox>
#include <QFile>
#include "barcode.h"

class PrintableWorksheet : public QObject
{
    Q_OBJECT
public:
    PrintableWorksheet(QWidget *parent=0);
    ~PrintableWorksheet();

    void addLine(QList<QString> item_data);
    void addPage();
    void setHeaders(QList<QString> header_list);
    void setDetails(QStringList details);
    void setFilePath(QString path, QString name);
    bool writeHtmlToFile(QString html);
    QString getFilePath();
    void printFile();

    int page_count;
    const int PAGE_BREAK_MARK = 14;

protected:
    void initTemplates();
    void initMembers();

    QString date;
    QStringList lines;
    QStringList pages;
    QStringList headers;
    QString html_template;
    QString line_template;
    QString page_template;
    QString file_path;

signals:
    void htmlSaved(QString file_path);
};

#endif // PRINTABLEWORKSHEET_H
