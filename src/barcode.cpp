#include "barcode.h"

Barcode::Barcode(QString sku)
{
    setBarcodeFont();

    barcode_string = encodeBarcode(sku);
    QPixmap pix = createBarcodeImage(barcode_string);
    barcode_html = createBarcodeHtml(pix);
}

void Barcode::setBarcodeFont()
{
    // If barcodes aren't scanning properly, they may be getting cut off on either end
    // Try reducing the font point size
    int id = QFontDatabase::addApplicationFont(":/font/code128.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    barcode_font.setFamily(family);
    barcode_font.setPointSize(250);
}

QString Barcode::encodeBarcode(QString code)
{
    QString encoded;

    encoded.prepend(QChar(codeToChar(CODE128_B_START)));
    encoded.append(code);
    encoded.append(QChar(calculateCheckCharacter(code)));
    encoded.append(QChar(codeToChar(CODE128_STOP)));

    return encoded;
}

int Barcode::calculateCheckCharacter(QString code)
{
    QByteArray encap_barcode(code.toUtf8());

    // Calculate check character
    long long sum = CODE128_B_START; // The sum starts with the B Code start character value
    int weight = 1; // Initial weight is 1

    foreach (char ch, encap_barcode) {
        int code_char = charToCode((int)ch); // Calculate character code
        sum += code_char*weight;
        weight++; // Increment weight
    }

    int remain = sum%103; // The check character is the modula 103 of the sum

    // Calculate the font integer from the code integer
    if(remain > 95) {
        remain += 100;
    } else {
        remain += 32;
    }

    return remain;
}

int Barcode::codeToChar(int code)
{
    return code + 100;
}

int Barcode::charToCode(int ch)
{
    return ch - 32;
}

QPixmap Barcode::createBarcodeImage(QString b_string)
{
    // Reducing the width in QPixMap and in QRect seems to make the barcode print slight bigger
    // but does not affect how zoomed in the barcode itself is (i.e., does not affect if anything is
    // cut off or not)
    QRect rect(0,0,1440,300);
    QPixmap pixmap(1450,300);
    pixmap.fill(Qt::white);

    QPainter *painter = new QPainter(&pixmap);
    painter->setFont(barcode_font);
    painter->setPen(Qt::black);
    painter->drawText(rect,Qt::AlignCenter,b_string);
    delete painter;

    return pixmap;
}

QString Barcode::createBarcodeHtml(QPixmap pix)
{
    QByteArray byte_array;
    QBuffer buffer(&byte_array);
    pix.save(&buffer, "PNG");

    QString url = QString("<img src='\data:image/png;base64,") + byte_array.toBase64() + "\' style='width:170px;height:25px;'/>";

    return url;
}

QString Barcode::getBarcodeHtml()
{
    return barcode_html;
}
