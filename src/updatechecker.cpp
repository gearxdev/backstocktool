#include "updatechecker.h"

UpdateChecker::UpdateChecker()
{
    setApplicationVersion();
    getUpdatesURL();

    s_updater = QSimpleUpdater::getInstance();
}

void UpdateChecker::getUpdatesURL()
{
    QSettings settings(CONFIG_FILE_PATH, QSettings::IniFormat);

    settings.beginGroup("updates");
    updates_url = settings.value("updates_url").toString();
    settings.endGroup();
}

void UpdateChecker::setApplicationVersion()
{
    QFile updates_file(UPDATES_FILE_PATH);

    if (!updates_file.open(QIODevice::ReadOnly|QIODevice::Text)) {
        QMessageBox msg_box;
        msg_box.setText("Error!: Failed to open updates file at " + updates_file.fileName());
        msg_box.exec();
        return;
    }

    QByteArray updates_data = updates_file.readAll();
    if (updates_data.isEmpty()) {
        QMessageBox msg_box;
        msg_box.setText("Error: failed to read updates file or it was empty.");
        msg_box.exec();
        return;
    }

    QJsonDocument updates_doc = QJsonDocument::fromJson(updates_data);
    QJsonObject doc_object = updates_doc.object();
    QJsonObject updates_object = doc_object.value("updates").toObject();
    QString os_type = QSysInfo::productType();
    QJsonObject os_object = updates_object.value(os_type).toObject();
    QString current_version = os_object.value("latest-version").toString();

    qApp->setApplicationVersion(current_version);
}

void UpdateChecker::checkForUpdates()
{
    // Get the application version
    QString version = qApp->applicationVersion();

    s_updater->setModuleVersion(updates_url, version);
    s_updater->setNotifyOnUpdate(updates_url, true);
    s_updater->setDownloaderEnabled(updates_url, true);

    s_updater->checkForUpdates(updates_url);
}

UpdateChecker::~UpdateChecker()
{

}
