# Description #

This program is designed to help warehouse receivers when backstocking recently received product.  In Posim Premier, item locations were printed on tags so the person backstocking could see where the item was last located and backstock it accordingly.  In Posim Evo, we cannot print locations on tags so receivers do not have an easily accessible reference for where product should be backstocked.  This program takes the RL number provided by the user and generates a PDF containing information about all the items in the receiving log that will help the user backstock everything in the correct warehouse location.

# Usage #

Once the user has finished receiving an order and is ready to backstock it, he/she enters the receiving log number into the program.  The program then displays the basic information related to the RL (Receiver, Vendor, and Date) for confirmation that it is the correct one.  If it is, the user then clicks the "Print Backstock Worksheet" button, which then saves and opens an html document which displays a table of the following information about each item received on the RL:

* Department
* Misc1
* Location
* Sku
* Quantity Received
* Quantity Available
* Brand
* Description
* Location

This html document is intended to be printed and used as a worksheet while backstocking.

# How It Works #

#### Qt Model/View Approach ####

This program uses Qt's Model/View architecture for handling and displaying the receiving log data (specifically, the QAbstractTableModel class).  Qt has excellent documentation on this below:

[Model/View Programming](http://doc.qt.io/qt-5/model-view-programming.html)

[QAbstractTableModel](http://doc.qt.io/qt-5/qabstracttablemodel.html)

#### Barcodes ####

The approach to printable barcodes in this program is based off of an application created by [Adam Giamcomelli](https://github.com/adamgiacomelli/Qt-barcode)

The barcode font used in this program is code128.  As far as I am aware, the only barcode fonts that our scanners can read are code128 and code39.

# Application Management (Windows Specific)#

## REQUIRED: ##
* Qt 5.10
* Microsoft Visual Studio 2017
* Qt Creator IDE setup to compile with MSVC2017
* NSIS
* Add C:\Qt\[Version]\[Compiler]\bin\ and C:\Program Files (x86)\NSIS\ to your PATH variable

## Compiling (Building) ##
This program is designed to be built via the Qt Creator IDE using Qt version 5.10 and the MSVC2017 (Microsoft Visual Compiler - bundled with Visual Studio) compiler.  The "shadow build" feature should be unselected for the build of this project (found via the Projects tab in the sidebar**. **Make sure that you do a final release build of the project before deploying.**

## Deploying ##
Deploying is done using the deploy_script.ps1 Powershell script found in the project folder.  It moves the exe to a deploy folder and runs windeployqt, which is Qt's tool to copy the necessary dependencies to be bundled with the program. It then copies in some DLLs that windeployqt is known to miss and are stored in the projects libs folder.  It then compiles the installer using makensis, NSIS's installer compiling command line tool (see below).

## Installer ##
The installer is created using NSIS (Nullsoft Scriptable Install System - http://nsis.sourceforge.net/Main_Page).  The installer script is compiled automatically by the deploy script, but if you want to compile it manually you can right-click the script and select "Compile NSIS Script".

## Updating ##

This program auto-updates using the third party library QSimpleUpdater ( https://github.com/alex-spataru/QSimpleUpdater ).  As a most basic distillation, this library checks the versioning of an installer on a server (indicated in a json file) and compares it to the version of the application (indicated in a local json file).  If the version on the server is greater than the version being run, it prompts the user to download an update if they wish.  They are then prompted to run the installer if they wish (see Installer portion for more details on this).  Update checking occurs via the UpdateChecker class as soon at the application is run.


# Caveats #

**Posted RLs Only** - Only RLs that have been posted will show up in this program.  If the RL isn't posted then the items aren't in inventory and therefore shouldn't be backstocked.

**Received Items Only** - Only items from the RL that have a received quantity greater than 0 will show up on the worksheet.  If an item was not received then it will not need to be backstocked and can therefore be ignored.

**Fickle Barcodes** - Getting the barcodes to be scannable was very challenging.  If you make the font too small the scanner can't read it, but if you make the font too large the ends of the barcode might get cut off.  If there are problems with the barcodes not scanning properly, try messing with the font size. Also, because the barcode image is represented as a QPixmap, any zoom/maginification present in Windows (including changing icon size, for some reason) will have the same zoom/magnification effect on the barcode image (the QPixmap [documentation](http://doc.qt.io/qt-5/qpixmap.html) states, "Note that the pixel data in a pixmap is internal and is managed by the underlying window system. "). This is likely to make the barcode cut off and unscannable.  When possible, it would be ideal to figure out a way to render the barcode that can't be affected by magnification.

# Possible Future Improvements #

#### Html Document Approach ####
Currently, the html is written to a file as a text stream.  This is because using QWebEnginePage (a more powerful and more 'Qt' way) to save the html to a PDF had a lot of problems with timing out on larger RLs and never generating the file.  For that reason, this program does not use QWebEnginePage like most of the other Qt programs do.  I could not figure out exactly why this was happening or how to fix it, but if someone does or it is found to be a Qt bug that gets fixed then it would make sense to match the approach of all the other programs.
