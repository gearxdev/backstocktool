!define REGUNINSTKEY "BackstockTool" ; GUID
!define REGHKEY HKLM ; admin privileges
!define REGPATH_WINUNINST "Software\Microsoft\Windows\CurrentVersion\Uninstall"
!define PROJDIR ".\deploy" ; Directory where project files current exist
!define PNAME "BackstockTool" ; Program name

# define installer name
Name "${PNAME} Installer"

# define name of installer executable file
OutFile "${PNAME}_Installer.exe"

# define installation directory
InstallDir "$PROGRAMFILES\${PNAME}"

# start default section
Section

  # close the program if it is open
  ExecWait "TaskKill /IM ${PNAME}.exe"

  # sleep for 2 seconds as a hack for slower machines (the grandchildren processes of taskkill are not waited for and do not finish quickly enough)
  Sleep 2000
  
  # show the installation details
  SetDetailsView show

  # autoclose the window after completion
  SetAutoClose true

  # set the installation directory as the destination for the following actions
  SetOutPath $INSTDIR

  # include files
  File /r "${PROJDIR}\*"

  # create the uninstaller
  WriteUninstaller "$INSTDIR\uninstaller.exe"

  # create a shortcut in the start menu and point at program
  CreateShortCut "$SMPROGRAMS\${PNAME}.lnk" "$INSTDIR\${PNAME}.exe"

  # add registry key for easy uninstalling via add/remove programs section in control panel
  WriteRegStr ${REGHKEY} "${REGPATH_WINUNINST}\${REGUNINSTKEY}" "DisplayName" "${PNAME}"
  WriteRegStr ${REGHKEY} "${REGPATH_WINUNINST}\${REGUNINSTKEY}" "UninstallString" '"$INSTDIR\uninstaller.exe"'

  # user friendly message box completion alert, open program if user chooses
  MessageBox MB_YESNO "The installation is complete! Would you like to open the program now?" IDYES open IDNO ignore
  open:
    Exec "$INSTDIR\${PNAME}.exe"
  ignore:
    # skip open command

SectionEnd

# uninstaller section start
Section "Uninstall"

  # delete the uninstaller
  Delete "$INSTDIR\uninstaller.exe"

  # delete the installer folder
  RMDir /r $INSTDIR

  # remove the link from the start menu
  Delete "$SMPROGRAMS\${PNAME}.lnk"

  # delete registry key
  DeleteRegKey ${REGHKEY} "${REGPATH_WINUNINST}\${REGUNINSTKEY}"

  # uninstaller section end
SectionEnd

