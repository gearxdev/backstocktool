#-------------------------------------------------
#
# Project created by QtCreator 2016-07-08T13:56:56
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(libs/QSimpleUpdater/QSimpleUpdater.pri)

TARGET = BackstockTool
TEMPLATE = app
CONFIG -= debug_and_release debug_and_release_target

QMAKE_MAC_SDK=macosx10.12

SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/receivinglogwidget.cpp \
    src/receivinglogdata.cpp \
    src/receiveditem.cpp \
    src/printableworksheet.cpp \
    src/barcode.cpp \
    src/updatechecker.cpp

HEADERS  += src/mainwindow.h \
    src/receivinglogwidget.h \
    src/receivinglogdata.h \
    src/receiveditem.h \
    src/printableworksheet.h \
    src/barcode.h \
    src/updatechecker.h

RESOURCES += \
    resources/resources.qrc

macx {
    QMAKE_LFLAGS += -Wl,-rpath,@loader_path/../,-rpath,@executable_path/../,-rpath,@executable_path/../Frameworks

    CONFIG_FILE.files = $$PWD/config/config.ini
    CONFIG_FILE.path = Contents/MacOS/config
    UPDATES_FILE.files = $$PWD/definitions/BackstockTool_updates.json
    UPDATES_FILE.path = Contents/MacOs/definitions
    QMAKE_BUNDLE_DATA += CONFIG_FILE UPDATES_FILE
}

win32 {
    CONFIG(debug, debug|release) {
        DESTDIR = $$PWD\build\debug
    }
    CONFIG(release, debug|release) {
        DESTDIR = $$PWD\build\release
    }

    OBJECTS_DIR = $${DESTDIR}/.obj
    MOC_DIR = $${DESTDIR}/.moc
    RCC_DIR = $${DESTDIR}/.rcc
    UI_DIR = $${DESTDIR}/.ui

    copyconfig.commands += $(COPY_DIR) $$shell_path($$PWD/config) $$shell_path($$DESTDIR/config)
    copydefinitions.commands += $(COPY_DIR) $$shell_path($$PWD/definitions) $$shell_path($$DESTDIR/definitions)
    first.depends = $(first) copyconfig copydefinitions
    export(first.depends)
    export(copyconfig.commands)
    export(copydefinitions.commands)
    QMAKE_EXTRA_TARGETS += first copyconfig copydefinitions
    
    RC_FILE = resources/BackstockTool.rc
}
