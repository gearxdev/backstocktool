# This powershell script should sit in top level of the BackstockTool project folder.
# It takes a release build of the program, gathers it and all the necessary dependencies into a deploy folder.
# It then compiles the NSIS installer script into an installer executable
# Requirements:
# Make sure that there is a release build of the program at .\build\release\
# Make sure that C:\Qt\[Version]\[Compiler]\bin\ and C:\Program Files (x86)\NSIS\ are added to your PATH variable

# Set Executable Name and folder paths
$executable = 'BackstockTool.exe'
$build_folder = ".\build\release"
$deploy_folder = ".\deploy"

# Remove the deploy folder if it exists
if (Test-Path $deploy_folder) {Remove-Item $deploy_folder -Recurse;}

# Create deploy folder
New-Item -ItemType Directory -Force -Path $deploy_folder

# Copy the program executable from the build folder to the deploy folder
Copy-Item "$build_folder\$executable" -Destination $deploy_folder

# Run windeployqt on exe in deploy folder
Write-Host "RUNNING WINDEPLOYQT......"
windeployqt --compiler-runtime "$deploy_folder\$executable"

# Copy config and definitions folders to deploy folder
Write-Host "COPYING DEPENDENCIES......"
Copy-Item .\config -Destination $deploy_folder\config -Recurse
Copy-Item .\definitions -Destination $deploy_folder\definitions -Recurse

# Copy dlls from libs folder to deploy folder
Copy-Item ".\libs\*.dll" -Destination $deploy_folder

# Compile installer script
Write-Host "COMPILING INSTALLER SCRIPT....."
makensis installer_script.nsi
